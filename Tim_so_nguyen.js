function test(num){
    var isPrime = true;
    if (num < 2) return false;
    for (var i; i<= Math.floor(num/2); i++){
        if (num % i == 0){
            return false;
        }
    }
    return isPrime;
}
function printPrimeNumber(){
    var numArr = document.getElementById("number_Ex2").value.split(" ");
    var n = numArr.length;
    // numArr.push(n);
    var nElems = " ";
    for (var i = 2; i <= n; i++){
        if(printPrimeNumber(numArr[i]) == 1){
            nElems += numArr[i] + " ";
        }
    }
    document.getElementById("result").innerHTML = nElems;
}
